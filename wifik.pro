# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
CONFIG += qdeclarative-boostable

# Add dependency to Symbian components
# CONFIG += qt-components

#INCLUDEPATH += /usr/include/glib-2.0 /usr/lib/glib-2.0/include

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    wmain.cpp \
    networkdetails.cpp

PKGCONFIG += osso-wlan

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qml/main.qml \
    qml/TittleBar.qml \
    qml/MainPage.qml \
    qml/AboutDialog.qml \
    qml/SettingsPage.qml

RESOURCES += \
    resources.qrc

HEADERS += \
    wmain.h \
    networkdetails.h
