#ifndef WMAIN_H
#define WMAIN_H

#include <QDeclarativeView>
#include <QDeclarativeEngine>
#include <QDeclarativeView>
#include <QDeclarativeContext>
#include <QSettings>
#include <QGraphicsObject>
#include <QSettings>
#include <QDebug>
#include <QProcess>
#include <QTimer>

#include "networkdetails.h"

class QDBusMessage;

class WMain : public QObject
{
    Q_OBJECT
public:
    explicit WMain(QWidget *parent = 0);

public slots:
    void scanNetworks();
    void processFinished(int exitcode);
    void debug(QString text);
    void addNetwork(QString essid, QString bssid, int channel, QString encryption, int quality, QString frequency);
    void clearNetworkList();
    void setScanFrequency(int freq);
    void setSwitchAutoScan(bool aScan);
    void timeOut();
    void stopTimer();
    void startTimer();

private:
    QDeclarativeView *view;
    QDeclarativeContext *rootContext;
    QSettings *settings;
    QProcess process;
    QTimer timer;
    QString color;
    bool autoScan;
    int scanFrequency;


    QList<QObject*> networksList;
};

#endif // WMAIN_H
