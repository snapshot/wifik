#include "wmain.h"

WMain::WMain(QWidget *parent){
    view = new QDeclarativeView();

    rootContext = view->rootContext();
    rootContext->setContextProperty("wMain", this);

    view->setAttribute(Qt::WA_OpaquePaintEvent);
    view->setAttribute(Qt::WA_NoSystemBackground);

    view->setSource(QUrl("qrc:/qml/main.qml"));

    view->showFullScreen();

    QSettings *settings= new QSettings(this);
    //color=(settings->value("color","green").toString());;
    autoScan=(settings->value("autoScan",true).toBool());;
    scanFrequency=(settings->value("scanFrequency",2).toInt());;
    delete settings;

    //rootContext->setContextProperty("color", color);
    rootContext->setContextProperty("autoScan", autoScan);
    rootContext->setContextProperty("scanFrequency", scanFrequency);
    //TODO: add option to change color

    QObject::connect(&process, SIGNAL(finished(int)),this,SLOT(processFinished(int)));

    scanNetworks();
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(timeOut()));
    if (autoScan){
        timer->start(scanFrequency*1000);
    }
}


void WMain::scanNetworks(){
    qDebug("scanning..");
    QStringList args;
    args.append("wlan0");
    args.append("scan");
    process.start("/sbin/iwlist",args);
}

void WMain::timeOut(){
    scanNetworks();
}

void WMain::stopTimer(){
    timer.stop();
}

void WMain::startTimer(){
    if (autoScan){
        timer.start(scanFrequency*1000);
    }
}

void WMain::processFinished(int exitcode){
    qDebug() << exitcode;
    if(exitcode==0){
        clearNetworkList();
        QStringList networkData;
        if (process.canReadLine()){
            QString line = process.readLine();
            if (line.contains("wlan0     Scan completed :")){
                while(process.canReadLine()){
                    line = process.readLine();
                    if (line.contains(" - Address: ")&&line.contains("Cell ")){
                        if (!networkData.isEmpty()){
                            //debug("--Cell found--");
                            QString searchTerm="";
                            QString essid="";
                            QString bssid="";
                            QString frequency="";
                            //QString signalLevel="";
                            QString encryption="";
                            int quality=0;
                            int channel=0;

                            bssid=networkData.first().mid(networkData.first().indexOf("Address: ")+9);

                            for (int var = 0; var < networkData.count(); ++var) {
                                QString line = networkData.at(var);

                                searchTerm="Channel:";
                                if (line.contains(searchTerm)){
                                    channel=line.mid(line.indexOf(searchTerm)+searchTerm.count()).toInt();
                                }
                                searchTerm="Frequency:";
                                if (line.contains(searchTerm)){
                                    frequency=line.mid(line.indexOf(searchTerm)+searchTerm.count());
                                    frequency=frequency.mid(0,frequency.indexOf("(Channel")).trimmed();
                                }

                                searchTerm="Quality=";
                                if (line.contains(searchTerm)){
                                    QString qualityString;
                                    qualityString=line.mid(line.indexOf(searchTerm)+searchTerm.count());
                                    //signalLevel=qualityString;
                                    //TODO: Clean parse signalLevel (remove quality)
                                    qualityString=qualityString.mid(0,qualityString.indexOf("Signal level")).simplified();
                                    float x=qualityString.mid(0,qualityString.indexOf("/")).toFloat();
                                    float y=qualityString.mid(qualityString.indexOf("/")+1).toFloat();
                                    float r=x/y;
                                    quality=r*100;
                                }

                                searchTerm="Encryption key:";
                                if (line.contains(searchTerm)){
                                    encryption=line.mid(line.indexOf(searchTerm)+searchTerm.count()).simplified();
                                }

                                searchTerm="ESSID:";
                                if (line.contains(searchTerm)){
                                    essid=line.mid(line.indexOf(searchTerm)+searchTerm.count()+1);
                                    essid=essid.mid(0,essid.length()-2);
                                }

                                if (!line.contains("ESSID:")&&line.contains("WPA")){
                                    encryption="wpa";
                                }
                            }

                            if (encryption=="on"){
                                encryption="wep";
                            }

                            addNetwork(essid,bssid,channel,encryption,quality,frequency);
                            networkData.clear();
                        }
                        networkData.append(line);
                    }else{
                        networkData.append(line);
                    }
                }
            }else{
                qDebug() << line;
                //TODO: Show dialog saying no results if autoscan is set to no
            }
        }
    }else{
        //crash
    }
}

void WMain::debug(QString text){
    qDebug() << text;
}

void WMain::addNetwork(QString essid, QString bssid, int channel, QString encryption, int quality, QString frequency){
    networksList.append(new NetworkDetails(essid,bssid,channel,encryption,quality,frequency));
    rootContext->setContextProperty("networksModel", QVariant::fromValue(networksList));
}

void WMain::clearNetworkList(){
    networksList.clear();
    rootContext->setContextProperty("networksModel", QVariant::fromValue(networksList));
}

void WMain::setScanFrequency(int freq){
    scanFrequency=freq;
    scanNetworks();
    if (autoScan){
        timer.start(scanFrequency*1000);
    }else{
        timer.stop();
    }
    QSettings *settings= new QSettings(this);
    settings->setValue("scanFrequency",scanFrequency);
    delete settings;
}

void WMain::setSwitchAutoScan(bool aScan){
    autoScan=aScan;
    QSettings *settings= new QSettings(this);
    settings->setValue("autoScan",autoScan);
    delete settings;
}
