#include <QDebug>
#include "networkdetails.h"

NetworkDetails::NetworkDetails(QObject *parent)
    : QObject(parent)
{
}

NetworkDetails::NetworkDetails(const QString &essid, const QString &bssid, const int channel, const QString &encryption, const int &quality, const QString &frequency, QObject *parent)
    : QObject(parent), m_essid(essid), m_bssid(bssid), m_channel(channel), m_encryption(encryption), m_quality(quality), m_frequency(frequency)
{
}

QString NetworkDetails::essid() const
{
    return m_essid;
}

void NetworkDetails::setEssid(const QString &essid)
{
    if (essid != m_essid) {
        m_essid = essid;
        emit essidChanged();
    }
}

QString NetworkDetails::bssid() const
{
    return m_bssid;
}

void NetworkDetails::setBssid(const QString &bssid)
{
    if (bssid != m_bssid) {
        m_bssid = bssid;
        emit bssidChanged();
    }
}

int NetworkDetails::channel() const
{
    return m_channel;
}

void NetworkDetails::setChannel(const int channel){
    if (channel != m_channel) {
        m_channel = channel;
        emit channelChanged();
    }
}

QString NetworkDetails::encryption() const
{
    return m_encryption;
}

void NetworkDetails::setEncryption(const QString &encryption){
    if (encryption != m_encryption) {
        m_encryption = encryption;
        emit encryptionChanged();
    }
}

int NetworkDetails::quality() const{
    return m_quality;
}

void NetworkDetails::setQuality(const int &quality){
    if (quality != m_quality) {
        m_quality = quality;
        emit qualityChanged();
    }
}

QString NetworkDetails::frequency() const{
    return m_frequency;
}

void NetworkDetails::setFrequency(const QString &frequency){
    if (frequency != m_frequency) {
        m_frequency = frequency;
        emit frequencyChanged();
    }
}
