#ifndef NETWORKDETAILS_H
#define NETWORKDETAILS_H

#include <QObject>

class NetworkDetails : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString essid READ essid WRITE setEssid NOTIFY essidChanged)
    Q_PROPERTY(QString bssid READ bssid WRITE setBssid NOTIFY bssidChanged)
    Q_PROPERTY(int channel READ channel WRITE setChannel NOTIFY channelChanged)
    Q_PROPERTY(QString encryption READ encryption WRITE setEncryption NOTIFY encryptionChanged)
    Q_PROPERTY(int quality READ quality WRITE setQuality NOTIFY qualityChanged)
    Q_PROPERTY(QString frequency READ frequency WRITE setFrequency NOTIFY frequencyChanged)

public:
    NetworkDetails(QObject *parent=0);
    NetworkDetails(const QString &essid, const QString &bssid, const int channel, const QString &encryption, const int &quality, const QString &frequency, QObject *parent=0);

    QString essid() const;
    void setEssid(const QString &essid);

    QString bssid() const;
    void setBssid(const QString &bssid);

    int channel() const;
    void setChannel(const int channel);

    QString encryption() const;
    void setEncryption(const QString &encryption);

    int quality() const;
    void setQuality(const int &quality);

    QString frequency() const;
    void setFrequency(const QString &frequency);

signals:
    void essidChanged();
    void bssidChanged();
    void channelChanged();
    void encryptionChanged();
    void qualityChanged();
    void frequencyChanged();

private:
    QString m_essid;
    QString m_bssid;
    int m_channel;
    QString m_encryption;
    QString m_frequency;
    int m_quality;
};

#endif // NETWORKDETAILS_H
