import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0

Page {
    id: mainPage

    tools: commonTools

    TittleBar{
        id: tittleBar;
        tittle: "WiFik";
        author: "By Iktwo";
        color: "#1C1C1C";
        //tittleLink: "http://store.ovi.mobi/content/213823";
        authorLink: "http://store.ovi.mobi/publisher/Iktwo/";
        z: 1;
    }

    /*InfoBanner{
        id: bannerFullPath
        text: actualDir
        timerEnabled: true
        timerShowTime: 3500
        z: 1
        topMargin: 90+10
        //height: 100;
    }*/

    ListView {
        id: listview
        anchors.top: tittleBar.bottom
        anchors.bottom: parent.bottom
        width: mainPage.width
        model: networksModel
        cacheBuffer: 200;

        delegate: ListDelegate{
            Label {
                id: lblEssid;
                text: essid;
                font.pixelSize: 28
                anchors.top: parent.top
                anchors.left: imgLock.right
                anchors.leftMargin: 3
            }

            Label{
                id: lblEncryption
                text: encryption
                visible: false
                onTextChanged: {
                    if (lblEncryption.text=="off"){
                        imgLock.source=""
                    }else if (lblEncryption.text=="wpa"){
                        imgLock.source="qrc:/images/wpa.png"
                    }else if (lblEncryption.text=="wep"){
                        imgLock.source="qrc:/images/wep.png"
                    }
                }
            }
            Image {
                id: imgLock
                height: 32
                width: 32
                anchors.top: parent.top
                anchors.left: parent.left
            }

            Label {
                id: lblBssid;
                text: bssid;
                //font.pixelSize: 26
                anchors.left: imgLock.right
                anchors.top: lblEssid.bottom
            }

            Rectangle{
                color: "green"
                height: 15;
                width: 300;
                anchors.left: parent.left
                anchors.leftMargin: imgLock.width
                anchors.bottom: parent.bottom
            }

            Rectangle{
                color: "lime"
                height: 15;
                width: quality*3
                anchors.left: parent.left
                anchors.leftMargin: imgLock.width
                anchors.bottom: parent.bottom
                z:1
            }

            Label {
                id: lblChannel;
                text: "Ch "+channel;
                //font.pixelSize: 25
                anchors.right: parent.right
                anchors.top: parent.top
                //anchors.verticalCenter: parent.verticalCenter
                //anchors.verticalCenter: parent.bottom
                //anchors.verticalCenterOffset: -height/2
            }
        }
    }

    ScrollDecorator {
        flickableItem: listview
    }
}
