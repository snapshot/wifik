import QtQuick 1.0
import com.nokia.meego 1.0

QueryDialog {
    id: aboutDialog
    titleText: "WiFik by Iktwo " + "1.0.0"
    icon: "qrc:/images/iktwo.png"
    message: qsTr("A simple WiFi scanner.. See info about wireless networks around you. <br><br> If you like my work please support me buying my apps and giving me feedback.<br><br> &copy; Iktwo 2011<br><br>Bugs/Comments <br> Contact me <br><br> <a href=\"mailto:elmaildeliktwo@gmail.com\"> Email:</a> elmaildeliktwo@gmail.com <br><br> Twitter: @iktwo")
}
