import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
    id: appWindow

    initialPage: mainPage

    MainPage {
        id: mainPage
        Component.onCompleted:{
            theme.inverted = !theme.inverted;
        }
    }

    ToolBarLayout {
        id: commonTools
        visible: true
        ToolIcon {
            platformIconId: "toolbar-settings"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked:{
                pageStack.push(settingsPage);
                wMain.stopTimer();
            }
        }

        ToolIcon {
            Image {
                id: name
                x: parent.width/2-width/2
                y: parent.height/2 - height/2
                source: "qrc:/images/scan.png"
            }
            anchors.left: (parent === undefined) ? undefined : parent.left
            onClicked:{
                wMain.scanNetworks();
            }
        }
    }

    Menu {
        id: myMenu
        visualParent: pageStack
        MenuLayout {
            MenuItem {
                text: qsTr("About")
                onClicked: {
                    aboutDialog.open();
                }
            }
        }
    }

    AboutDialog{
        id: aboutDialog;
    }

    SettingsPage{
        id: settingsPage;
    }
}
