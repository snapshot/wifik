import QtQuick 1.1
import com.nokia.meego 1.0

Page {
    tools: settingsTools

    ToolBarLayout {
        id: settingsTools
        visible: false
        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked:{
                (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
            }
        }

        ToolIcon {
            platformIconId: "toolbar-back"
            anchors.left: (parent === undefined) ? undefined : parent.left
            onClicked:{
                wMain.setScanFrequency(sldFreq.value)
                pageStack.pop();
                myMenu.close();
                wMain.startTimer();
            }
        }
    }

    TittleBar{
        id: tittleBar;
        tittle: "WiFik";
        author: "Settings";
        color: "#1C1C1C";
        authorLink: "http://store.ovi.mobi/publisher/Iktwo/";
        z: 1;
    }

    Label {
        id: lblAutoScan
        text: "Auto Scan"
        anchors.left: parent.left
        anchors.leftMargin: 10
        anchors.verticalCenter: switchAutoScan.verticalCenter
    }

    Switch {
        id: switchAutoScan;
        checked: autoScan;
        anchors.top: tittleBar.bottom
        anchors.topMargin: 10
        anchors.left: lblAutoScan.right
        anchors.leftMargin: 10
        platformStyle: SwitchStyle {
            switchOn: "image://theme/color2-meegotouch-switch-on"
        }

        onCheckedChanged: {
            wMain.setSwitchAutoScan(switchAutoScan.checked);
            if (switchAutoScan.checked){
                wMain.startTimer();
            }else{
                wMain.stopTimer();
            }
        }
    }


    Slider{
        id: sldFreq;
        width: parent.width-20
        anchors.horizontalCenter: parent.horizontalCenter
        valueIndicatorText: value+"s";
        minimumValue: 1;
        maximumValue: 120;
        stepSize: 1;
        value: scanFrequency;
        valueIndicatorVisible: true;

        anchors.top: switchAutoScan.bottom
        anchors.topMargin: 10

        platformStyle: SliderStyle{
            grooveItemElapsedBackground:"image://theme/color2-meegotouch-slider-elapsed-background-horizontal"
        }
    }
}
